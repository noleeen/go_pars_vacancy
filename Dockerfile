FROM golang:1.18

WORKDIR /app

COPY . .

RUN go build -o /service ./cmd/app/main.go

CMD ["/service"]