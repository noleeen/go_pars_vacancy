package mongodb

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go_pars_vacancy/internal/config"
)

func NewMongoClient(ctx context.Context, cf config.Config) (*mongo.Database, error) {
	mongodbURL := fmt.Sprintf("mongodb://%s:%s", cf.MongoDB.Host, cf.MongoDB.Port)

	clientOptions := options.Client().ApplyURI(mongodbURL)

	//connect
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return nil, fmt.Errorf("connect to mongodb failed. error: %v", err)
	}

	//ping
	if err := client.Ping(ctx, nil); err != nil {
		return nil, fmt.Errorf("ping to mongodb failed. error: %v", err)
	}
	return client.Database(cf.MongoDB.Database), nil
}
