package main

import (
	"context"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"go_pars_vacancy/internal/config"
	"go_pars_vacancy/internal/repository"
	"go_pars_vacancy/internal/service"
	"go_pars_vacancy/internal/transport"
	"go_pars_vacancy/pkg/cllient/mongodb"
	"go_pars_vacancy/pkg/cllient/postgresql"
)

func main() {
	port := ":8080"
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	vacancyHandler := transport.GetHandlerVacancy(selectDB())
	swaggerHandler := transport.GetHandlerSwagger()

	vacancyHandler.Register2(r)
	swaggerHandler.Register2(r)

	transport.RunServer(r, port)
}

func selectDB() (db service.VacancyRepository) {
	cfg := config.GetConfig()

	switch cfg.SelectDb {
	case "mongodb":
		mongodbClient, err := mongodb.NewMongoClient(context.Background(), *cfg)
		if err != nil {
			panic(err)
		}
		db = repository.NewMongoStorageVacancy(mongodbClient, cfg.MongoDB.Collection)

	case "postgres":
		postgresClient, err := postgresql.NewPostgresClient(context.TODO(), 3, *cfg)
		if err != nil {
			panic(err)
		}
		db = repository.NewPostgresStorageVacancy(postgresClient)
	default:
		db = repository.NewVacancyRep()
	}
	return
}
