package service

import (
	"go_pars_vacancy/internal/model"
)

type VacancyRepository interface {
	Create(dto model.Vacancy) error
	GetById(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type VacancyServiceS struct {
	rep VacancyRepository
}

func NewVacancyService(rep VacancyRepository) *VacancyServiceS {
	return &VacancyServiceS{rep}
}

func (v *VacancyServiceS) ServiceVacancyCreate(dto model.Vacancy) error {
	return v.rep.Create(dto)
}

func (v *VacancyServiceS) ServiceVacancyGetById(id int) (model.Vacancy, error) {
	return v.rep.GetById(id)
}

func (v *VacancyServiceS) ServiceVacancyGetList() ([]model.Vacancy, error) {
	return v.rep.GetList()
}

func (v *VacancyServiceS) ServiceVacancyDelete(id int) error {
	return v.rep.Delete(id)
}
