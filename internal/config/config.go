package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"log"
	"sync"
)

type Config struct {
	IsDebug   *bool `yaml:"is_debug" env-required:"true"` //env-required - обязательное поле
	Listening struct {
		Type   string `yaml:"type" env-default:"port"` //env-default - значение по умолчанию
		BindIP string `yaml:"bind_ip" env-default:"127.0.0.1"`
		Port   string `yaml:"port" env-default:"8080"`
	} `yaml:"listening"`

	SelectDb string `json:"select_db"`

	MongoDB struct {
		Host       string `json:"host"`
		Port       string `json:"port"`
		Database   string `json:"database"`
		AuthDB     string `json:"authDB"`
		Username   string `json:"username"`
		Password   string `json:"password"`
		Collection string `json:"collection"`
	} `json:"mongodb"`

	Postgres struct {
		Host     string `json:"host"`
		Port     string `json:"port"`
		Database string `json:"database"`
		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"postgres"`
}

var instance *Config
var once sync.Once

func GetConfig() *Config {
	once.Do(func() { // once.Do означает что этот код выполнится только один раз.
		// при повторном вызове фуункции код выполняться не будет, сразу вызовется инстансе
		log.Println("read application configuration")
		instance = &Config{} //создает новый объект Config и присваивает его адрес переменной instance.
		// Объект Config представляет конфигурацию приложения. /home/noleen/go/src/go_pars_vacancy/config.yml
		if err := cleanenv.ReadConfig("config.yml", instance); err != nil { //считывает конфигурацию приложения из файла

			help, _ := cleanenv.GetDescription(instance, nil) //получает описание структуры Config с помощью
			// функции GetDescription() из пакета cleanenv.
			//Описание может использоваться для вывода справки или информации о конфигурации.

			log.Println(help)
			log.Fatal(err)
		}
	})
	return instance
}
