package transport

import (
	"encoding/json"
	"fmt"
	"go_pars_vacancy/internal/model"
	"go_pars_vacancy/internal/service"
	"go_pars_vacancy/pkg/parser"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
)

type VacancyService interface {
	ServiceVacancyCreate(dto model.Vacancy) error
	ServiceVacancyGetById(id int) (model.Vacancy, error)
	ServiceVacancyGetList() ([]model.Vacancy, error)
	ServiceVacancyDelete(id int) error
}

type VacancyParser interface {
	ParseStart() ([]model.Vacancy, error)
}

type handlerVacancy struct {
	service VacancyService
}

func GetHandlerVacancy(rep service.VacancyRepository) Handlers {
	return &handlerVacancy{service: service.NewVacancyService(rep)}
}

func (h *handlerVacancy) Register2(r chi.Router) {
	r.Post("/parse", h.NewVacancyParse)
	r.Get("/vacancy/{vacancyId}", h.VacancyGetById)
	r.Post("/vacancy", h.AddOne)
	r.Get("/vacancies", h.VacanciesGet)
	r.Delete("/vacancy/{vacancyId}", h.VacanciesDelete)
}

func (h *handlerVacancy) AddOne(w http.ResponseWriter, r *http.Request) {
	var newVacancy model.Vacancy
	if err := json.NewDecoder(r.Body).Decode(&newVacancy); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
		return
	}
	if err := h.service.ServiceVacancyCreate(newVacancy); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
		return
	}
	_, err := fmt.Fprintf(w, "user with id: %v created\n", newVacancy.Identifier.Value)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}

}

func (h *handlerVacancy) NewVacancyParse(w http.ResponseWriter, r *http.Request) {
	newParser := parser.NewParserVacancy()
	vacancies, err := newParser.ParseStart()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}
	for _, vacancy := range vacancies {
		if err := h.service.ServiceVacancyCreate(vacancy); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	_, err = fmt.Fprint(w, "the bird in the cage")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}
}

func (h *handlerVacancy) VacancyGetById(w http.ResponseWriter, r *http.Request) {
	vacancyIdRaw := chi.URLParam(r, "vacancyId")
	vacancyId, err := strconv.Atoi(vacancyIdRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
		return
	}
	vacancy, err := h.service.ServiceVacancyGetById(vacancyId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound) //404
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode(vacancy); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
	}
}

func (h *handlerVacancy) VacanciesGet(w http.ResponseWriter, r *http.Request) {
	vacancies, err := h.service.ServiceVacancyGetList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound) //404
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode(vacancies); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
	}

}

func (h *handlerVacancy) VacanciesDelete(w http.ResponseWriter, r *http.Request) {
	vacancyIdRaw := chi.URLParam(r, "vacancyId")
	vacancyId, err := strconv.Atoi(vacancyIdRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
		return
	}
	err = h.service.ServiceVacancyDelete(vacancyId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound) //404
		return
	}

	_, err = fmt.Fprintf(w, "vacancy with id: %d deleted\n", vacancyId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}

}
