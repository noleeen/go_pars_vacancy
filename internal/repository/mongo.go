package repository

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go_pars_vacancy/internal/model"
	"go_pars_vacancy/internal/service"
	"log"
	"strconv"
)

type mongoStorageVacancy struct {
	collection *mongo.Collection
}

func NewMongoStorageVacancy(database *mongo.Database, nameCollection string) service.VacancyRepository {
	return &mongoStorageVacancy{collection: database.Collection(nameCollection)}
}

func (m *mongoStorageVacancy) Create(dto model.Vacancy) error {
	//id, err := strconv.Atoi(dto.Identifier.Value)
	//if err != nil {
	//	return fmt.Errorf("[mongo] error convert id: %v",err)
	//}
	_, err := m.collection.InsertOne(context.TODO(), dto)
	if err != nil {
		return fmt.Errorf("[mongo] error create user with id: %s | error: %v", dto.Identifier.Value, err)
	}
	return nil
}

func (m *mongoStorageVacancy) GetById(id int) (model.Vacancy, error) {

	var vacancy model.Vacancy
	idStr := strconv.Itoa(id)

	result := m.collection.FindOne(context.TODO(), bson.M{"identifier.vacancy_id": idStr})
	if result.Err() != nil {
		return model.Vacancy{}, fmt.Errorf("[mongo] not find vacancy with id: %d  |  err: %v", id, result.Err())
	}
	if err := result.Decode(vacancy); err != nil {
		return model.Vacancy{}, fmt.Errorf("[mongo] failed to decode vacancy id: %d from DB  |  err: %v", id, err)
	}

	return vacancy, nil
}

func (m *mongoStorageVacancy) GetList() ([]model.Vacancy, error) {
	var vacancsies []model.Vacancy

	findOptions := options.Find()
	cursor, err := m.collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		log.Fatal(err)
	}

	for cursor.Next(context.TODO()) {
		var vacancy model.Vacancy
		err := cursor.Decode(&vacancy)
		if err != nil {
			log.Fatal(err)
		}
		vacancsies = append(vacancsies, vacancy)
	}
	return vacancsies, nil
}

func (m *mongoStorageVacancy) Delete(id int) error {
	idStr := strconv.Itoa(id)
	delResult, err := m.collection.DeleteOne(context.TODO(), bson.M{"identifier.vacancy_id": idStr})
	if err != nil {
		return fmt.Errorf("[mongo] failed to delete vacancy id: %d from DB  |  err: %v", id, err)
	}

	if delResult.DeletedCount == 0 {
		return fmt.Errorf("[mongo] not find vacancy for delete with id: %d", id)
	}
	log.Println("deleted vacancy with id:", id)

	return nil
}
